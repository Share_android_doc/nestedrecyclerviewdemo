package com.somee.rothnaksomang.recycleview_version2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.somee.rothnaksomang.recycleview_version2.adapter.RecycleViewDataAdapter;
import com.somee.rothnaksomang.recycleview_version2.entity.SectionDataModel;
import com.somee.rothnaksomang.recycleview_version2.entity.SingleItemModel;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private ArrayList<SectionDataModel> allSampleData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        allSampleData=new ArrayList<>();

        createDummyData();
       // Log.e("DataMain=>",allSampleData.toString());
        RecyclerView recyclerView=findViewById(R.id.rv_allitem);
        recyclerView.setHasFixedSize(true);
        RecycleViewDataAdapter adapter=new RecycleViewDataAdapter(allSampleData,this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        recyclerView.setAdapter(adapter);
    }
    public void createDummyData(){
        for(int i=1;i<=20;i++){
            SectionDataModel sdm=new SectionDataModel();
            sdm.setHeaderTitle("Section"+1);
            ArrayList<SingleItemModel> singleItemModels=new ArrayList<>();
            for(int j=1;j<=20;j++){
                singleItemModels.add(new SingleItemModel("Item "+j,"URL "+j));
            }
            sdm.setAllItemInSection(singleItemModels);
            allSampleData.add(sdm);

        }
    }
}
