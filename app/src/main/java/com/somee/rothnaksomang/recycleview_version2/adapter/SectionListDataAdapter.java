package com.somee.rothnaksomang.recycleview_version2.adapter;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.somee.rothnaksomang.recycleview_version2.R;
import com.somee.rothnaksomang.recycleview_version2.entity.SingleItemModel;

import java.util.List;

public class SectionListDataAdapter extends RecyclerView.Adapter<SectionListDataAdapter.SingleItemRowHolder>{

    List<SingleItemModel> singleItemModelList;
    Context context;

    public SectionListDataAdapter(List<SingleItemModel> singleItemModelList, Context context) {
        this.singleItemModelList = singleItemModelList;
        this.context = context;
    }

    @NonNull
    @Override
    public SingleItemRowHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_adapter,null);
        return new SingleItemRowHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SingleItemRowHolder singleItemRowHolder, int i) {
        SingleItemModel singleItemModel = singleItemModelList.get(i);
    }

    @Override
    public int getItemCount() {
        return singleItemModelList.size();
    }

    public class SingleItemRowHolder extends RecyclerView.ViewHolder{

        TextView tvTitle;
        ImageView itemImage;

        public SingleItemRowHolder(@NonNull View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            itemImage = itemView.findViewById(R.id.ivImage);
        }
    }

}
