package com.somee.rothnaksomang.recycleview_version2.adapter;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.somee.rothnaksomang.recycleview_version2.R;
import com.somee.rothnaksomang.recycleview_version2.entity.SectionDataModel;
import com.somee.rothnaksomang.recycleview_version2.entity.SingleItemModel;
import java.util.List;


public class RecycleViewDataAdapter extends RecyclerView.Adapter<RecycleViewDataAdapter.ItemRowHolder>{

    List<SectionDataModel> list;
    Context context;
    RecyclerView.RecycledViewPool recycledViewPool;

    public RecycleViewDataAdapter(List<SectionDataModel> list, Context context) {
        this.list = list;
        this.context = context;
        recycledViewPool = new RecyclerView.RecycledViewPool();
    }

    @NonNull
    @Override
    public ItemRowHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_adapter_horizontal_scroll, null);
        return new ItemRowHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemRowHolder itemRowHolder, int i) {
                String sectionName = list.get(i).getHeaderTitle();
                List<SingleItemModel> itemModel = list.get(i).getAllItemInSection();

                itemRowHolder.itemTitle.setText(sectionName);

                SectionListDataAdapter adapter = new SectionListDataAdapter(itemModel, context);
                itemRowHolder.recyclerView.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false));
                itemRowHolder.recyclerView.setAdapter(adapter);
                itemRowHolder.recyclerView.setRecycledViewPool(recycledViewPool);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ItemRowHolder extends RecyclerView.ViewHolder{

        TextView itemTitle;
        RecyclerView recyclerView;
        Button btnMore;
        public ItemRowHolder(@NonNull View itemView) {
            super(itemView);
            itemTitle = itemView.findViewById(R.id.tvMainTitle);
            recyclerView = itemView.findViewById(R.id.rv_item);
            btnMore = itemView.findViewById(R.id.btn_more);
        }
    }
}
